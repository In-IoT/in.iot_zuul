## In.IoT Zuul Service

In.IoT is a middleware platform for Internet of Things (IoT) from an ongoing research project at the Inatel IoT Research Group. It is a contribution that represents a new concept of connecting IoT that is simple to deploy, use, and share.

This is the Zuul gateway service.

[You can find the user guide here](https://bitbucket.org/In-IoT/in.iot-device-api/wiki/Usage%20Guide)

[You can find installation instructions in our website](https://inatel.br/in-iot/)

Or in our Wiki:

* [Ubuntu](https://bitbucket.org/In-IoT/moquette-in.iot/wiki/installation%20(Ubuntu))
* Windows (Comming soon)
* MAC (Comming soon)
* CentOS (Comming soon)

[To secure your deployment and scale the solution with Microservices access this tutorial](https://bitbucket.org/In-IoT/moquette-in.iot/wiki/scaling%20and%20securing%20the%20solution)
[To configure your MQTT cluster access this tutorial](https://bitbucket.org/In-IoT/moquette-in.iot/wiki/Configuring%20Multiple%20MQTT%20Brokers)


## Ports 

| Application   				| Port 			|
| ------------- 				| ------------- |
| Device API  					| 8070  		|
| Admin GUI						| 8100  		|
| Admin REST API  				| 8190  		|
| MQTT Broker  					| 1883  		|
| Eureka Service discovery  	| 8761  		|
| Zuul Gateway  				| 8090  		|
| MQTT Proxy  					| 5000  		|
| CoAP Server  					| 5683  		|
| CoAP Proxy  					| 5001  		|


## What is Zuul?


<img src="https://i.imgur.com/mRSosEp.png" width=500/>

[![Build Status](https://travis-ci.org/Netflix/zuul.svg)](https://travis-ci.org/Netflix/zuul/builds)

Zuul is an edge service that provides dynamic routing, monitoring, resiliency, security, and more.
Please view the wiki for usage, information, HOWTO, etc https://github.com/Netflix/zuul/wiki

Here are some links to help you learn more about the Zuul Project. Feel free to PR to add any other info, presentations, etc.

---

Articles from Netflix:

Zuul 1: http://techblog.netflix.com/2013/06/announcing-zuul-edge-service-in-cloud.html

Zuul 2: http://techblog.netflix.com/2016/09/zuul-2-netflix-journey-to-asynchronous.html

---

Netflix presentations about Zuul:

Strange Loop 2017 - Zuul 2: https://youtu.be/2oXqbLhMS_A

AWS re:Invent 2018 - Scaling push messaging for millions of Netflix devices: https://youtu.be/IdR6N9B-S1E
 
---

Slides from Netflix presentations about Zuul:

http://www.slideshare.net/MikeyCohen1/zuul-netflix-springone-platform

http://www.slideshare.net/MikeyCohen1/rethinking-cloud-proxies-54923218

https://github.com/strangeloop/StrangeLoop2017/blob/master/slides/ArthurGonigberg-ZuulsJourneyToNonBlocking.pdf

https://www.slideshare.net/SusheelAroskar/scaling-push-messaging-for-millions-of-netflix-devices

---

Projects Using Zuul:

https://cloud.spring.io/

https://jhipster.github.io/

---

Info and examples from various projects:

https://spring.io/guides/gs/routing-and-filtering/

http://www.baeldung.com/spring-rest-with-zuul-proxy

https://blog.heroku.com/using_netflix_zuul_to_proxy_your_microservices

http://kubecloud.io/apigatewaypattern/

http://blog.ippon.tech/jhipster-3-0-introducing-microservices/

---

Other blog posts about Zuul:

https://engineering.riotgames.com/news/riot-games-api-fulfilling-zuuls-destiny

https://engineering.riotgames.com/news/riot-games-api-deep-dive

http://instea.sk/2015/04/netflix-zuul-vs-nginx-performance/

---