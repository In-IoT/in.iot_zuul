package br.inatel.zuul.In.IoT_Zuul;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;


@SpringBootApplication
@EnableZuulProxy
@EnableDiscoveryClient
public class RunZuul extends SpringBootServletInitializer {
	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(RunZuul.class);
	}
	public static void main(String[] args) {
		checkIfConfigFileExists();
		SpringApplication.run(RunZuul.class, args);
	}
	
	private static void checkIfConfigFileExists() {

		String filename = System.getProperty("user.dir")+ File.separator + "config" + File.separator + "application.properties";
		File file = new File(filename);
		if (!file.exists()) {
			file.mkdirs();
			file.delete();
			try {
				file.createNewFile();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try (Writer writer = new BufferedWriter(
					new OutputStreamWriter(new FileOutputStream(filename), "utf-8"))) {
				writer.write("# Give a name to the Zuul Gateway\n" + 
						"spring.application.name=In.IoT-zuul-server\n" + 
						"spring.cloud.refresh.enabled=false\n" + 
						"\n" + 
						"#Port for Zuul Gateway\n" + 
						"server.port=8090\n" + 
						"#server.connection-timeout=10000\n" + 
						"eureka.instance.preferIpAddress=true\n" + 
						"eureka.client.registerWithEureka=true\n" + 
						"eureka.client.fetchRegistry=true\n" + 
						"eureka.client.serviceUrl.defaultZone=http://inIoTTest:lucasabbadeWare@localhost:8761/eureka/\n" + 
						"management.security.enabled=false\n" + 
						"security.basic.enabled=false\n" + 
						"\n" + 
						"zuul.eureka.device-api.semaphore.maxSemaphores= 1000000\n" + 
						"zuul.eureka.dashboard-api.semaphore.maxSemaphores= 1000000\n" + 
						"zuul.sensitiveHeaders=none\n" + 
						"zuul.add-host-header=true\n" + 
						"zuul.addProxyHeaders = false\n" + 
						"zuul.set-content-length=true\n" + 
						"\n" + 
						"\n" + 
						"ribbon.maxAutoRetries = 1\n" + 
						"\n" + 
						"# Connect timeout used by Apache HttpClient\n" + 
						"ribbon.ConnectTimeout=3000\n" + 
						"\n" + 
						"# Read timeout used by Apache HttpClient\n" + 
						"ribbon.ReadTimeout=5000");
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}


